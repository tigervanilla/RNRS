var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
   showHomePage: function* (next) {


        /*var userId= this.request.query.id;
        var queryString='select * from user where u_id ="%s"';
        var query=util.format(queryString,userId);
        var result=yield databaseUtils.executeQuery(query);*/


        var queryString_most_review ='SELECT p.name, p.image_url, COUNT(*) as "rated_by", AVG(r.rating) as "rating" FROM product AS p LEFT JOIN review AS r ON p.p_id = r.p_id where r.review is not null GROUP BY p.p_id ORDER BY count(*) DESC LIMIT 5';
        var result1=yield databaseUtils.executeQuery(queryString_most_review);

        var queryString_top_rated ='SELECT p.name, p.image_url, COUNT(*) as "rated_by", AVG(r.rating) as "rating" FROM product AS p LEFT JOIN review AS r ON p.p_id = r.p_id GROUP BY p.p_id ORDER BY AVG(r.rating) DESC LIMIT 5';
        var result2=yield databaseUtils.executeQuery(queryString_top_rated);

        var queryString_top_distributers='SELECT v.firm_name,v.logo, v.location, COUNT(*) as "added_product" FROM vendor AS v LEFT JOIN vendor_product AS vp ON v.u_id = vp.u_id GROUP BY v.u_id ORDER BY count(*) DESC LIMIT 3';
        var result3=yield databaseUtils.executeQuery(queryString_top_distributers);
        /*var queryString='select * from user where name ="%s"';
        var query=util.format(queryString,'kumud');
        var result=yield databaseUtils.executeQuery(query);*/

        yield this.render('home',{
            showDetails:false,
            userDetails1:result1,
            userDetails2:result2,
            userDetails3:result3
        });
    },
    showtestPage: function* (next) {
        yield this.render('test',{
            userDetails:'this is my change'
        });
    },

    showproductPage: function* (next) {



        yield this.render('gla',{

        });
    },
    showglaPage: function* (next) {
        yield this.render('gla',{

        });
    },
    showProductPage: function* (next) {

        userId= this.request.query.id;
        var queryString_Details = 'select * from product where p_id = "%s"';
        var query1 = util.format(queryString_Details, userId);// for dynamic parameters
        var result4 = yield databaseUtils.executeQuery(query1);
        console.log(result4);
        var queryString_avg_rating = 'SELECT AVG(rating) as avg FROM review WHERE p_id="%s"';
        var query2 = util.format(queryString_avg_rating, userId);// for dynamic parameters
        var result5 = yield databaseUtils.executeQuery(query2);
        console.log(result5[0]);
        
        var queryString_review = 'select feedback.id, name ,rating,review,feedback,count(feedback) AS count,review.creation_timestamp from (review inner join user on user.u_id=review.u_id ) left join feedback on review.id=feedback.id where p_id="%s" and verified = 0 GROUP BY id,feedback ORDER BY id,feedback';
        var query3 = util.format(queryString_review, userId);// for dynamic parameters
        var result6 = yield databaseUtils.executeQuery(query3);
        var fr = {};
        for(var ele in result6){
        console.log(result6[ele]);
        var id = result6[ele]['id'];
        var name = result6[ele]['name'];
        var rating = result6[ele]['rating'];
        var review = result6[ele]['review'];
        var feedback = result6[ele]['feedback'];
        var count = result6[ele]['count'];
        var creation_timestamp = result6[ele]['creation_timestamp'];
        if(fr[id]==undefined){
        fr[id]={};
        fr[id]['name']=name;
        fr[id]['rating']=rating;
        fr[id]['review']=review;
        fr[id]['creation_timestamp']=creation_timestamp;
        console.log(fr[id]);
        fr[id][feedback]=count;
        console.log(fr);
        }
        else{
        fr[id][feedback]=count;
        }
        
        }
        console.log(fr);
        user_reviews=[];
        for(var i in fr){
        var tmp={};
        console.log(i);
        tmp['id']=i;
        tmp['details']=fr[i];
        user_reviews.push(tmp);
        }
        console.log(user_reviews);
        //console.log(result5);
        var queryString_vendor = 'SELECT * FROM vendor where u_id in (Select distinct(u_id) from Product JOIN vendor_product ON product.p_id=vendor_product.p_id where product.p_id="%s")';
        var query4 = util.format(queryString_vendor, userId);// for dynamic parameters
        var result7 = yield databaseUtils.executeQuery(query4);
        console.log(result7[0]);
        // console.log(result);
        
        
        yield this.render('product',{
        userDetails4 : result4[0],
        userDetails5 : result5,
        userDetails6 : result6,
        userDetails7 : result7,
        user_reviews : user_reviews
        
        });
   },
   searchPage: function* (next) {
    var pnam = this.request.query.pname;
    var vnam = this.request.query.vname;

    
    
    if(pnam)
    {
    var queryproduct = 'select * from product where name like "%s"';
    var query=util.format(queryproduct,"%"+pnam+"%");
    var ven = false;
    }
    else{
        var queryvendor = 'select * from vendor where firm_name like "%s"';
        var query=util.format(queryvendor,"%"+vnam+"%");
        var ven=true;
        console.log(query);
    }
    console.log(query);
    var result= yield databaseUtils.executeQuery(query);
    var proDetails=result;
    yield this.render('search',{ 
            product: proDetails,
            isvendor: ven
    });
},
    showcustomerPage: function* (next) {

        userId= this.request.query.id;
        var queryString_details ='select * from user where u_id ="%s"';
        var query=util.format(queryString_details,userId);

        var queryString_reviewed_product='SELECT p.name, p.image_url, COUNT(*) as "rated_by", AVG(r.rating) as "rating" ,r.creation_timestamp FROM product AS p JOIN review AS r ON p.p_id = r.p_id WHERE r.p_id IN (SELECT r.p_id FROM review GROUP BY r.u_id HAVING r.u_id = 101 ORDER BY creation_timestamp) GROUP BY p.p_id ORDER BY AVG(r.rating)';
        var result1=yield databaseUtils.executeQuery(queryString_reviewed_product);
    
        var result2=yield databaseUtils.executeQuery(query);
        yield this.render('customer',{
            userDetails1:result1,
            userDetails2:result2[0]
        });
    },
    showVendorPage: function* (next) {

        userId= this.request.query.id;
        var queryString_Details = 'select * from vendor join user on vendor.u_id=user.u_id where vendor.u_id="%s"';
        var query1 = util.format(queryString_Details, userId);// for dynamic parameters
        var result4 = yield databaseUtils.executeQuery(query1);
        console.log(result4[0]);
        
        var queryString_allproducts = 'select name,image_url,vendor_product.p_id,avg(rating) as avg,product.available, vendor_product.creation_timestamp from ((product join vendor_product on vendor_product.p_id=product.p_id) left join review on review.p_id=vendor_product.p_id) where vendor_product.u_id="%s" group by product.p_id;';
        var query2 = util.format(queryString_allproducts, userId);// for dynamic parameters
        var result5 = yield databaseUtils.executeQuery(query2);
        console.log(result5);
        
        var queryString_recent = 'SELECT name,image_url,creation_timestamp FROM product JOIN vendor_product ON product.p_id=vendor_product.p_id where u_id="%s" order by creation_timestamp limit 1';
        var query3 = util.format(queryString_recent, userId);// for dynamic parameters
        var result6 = yield databaseUtils.executeQuery(query3);
        console.log(result6[0]);
        
        yield this.render('vendor',{
        userDetails4 : result4[0],
        userDetails5 : result5,
        userDetails6 : result6[0]
        });
        },
    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});

        this.redirect('/');
    }
}
