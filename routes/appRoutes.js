var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();

    //Welcome Routes
    var welcomeCtrl = require('./../controllers/WelcomeCtrl');
    router.get('/home',welcomeCtrl.showHomePage);
    router.get('/test', welcomeCtrl.showtestPage);
   router.get('/gla', welcomeCtrl.showproductPage);
   router.get('/customer', welcomeCtrl.showcustomerPage);
   router.get('/product', welcomeCtrl.showProductPage);
router.get('/search', welcomeCtrl.searchPage);
router.get('/vendor',welcomeCtrl.showVendorPage);
    return router.middleware();
}
